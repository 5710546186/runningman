var RunningMan = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/dino1.png' );
        this.movingAction = this.createAnimationAction();
        this.deathAction = this.createDeathAnimation();
        this.number = 0;
        this.vy = 0;
        this.isJumping = false;
        this.isFalling = false;
        this.isAlive = true;
        this.floorY = 130;
        this.startAnime = false;
        this.isStart = false;
    },
    
    update: function( dt ) {
        this.checkDeath();
        if(this.isStart == true&& this.isAlive== true){
            SpeedObstacle = -7;
            if(this.startAnime == false){
                this.moveRight();
                this.startAnime = true;
            }
        }
        var pos = this.getPosition();
        this.setPosition( new cc.Point( pos.x , pos.y + this.vy) );
        if(this.isJumping&&!this.isFalling){
            if(pos.y<=240)
                this.vy = RunningMan.JUMP;
            else {
                this.setPosition( new cc.Point( pos.x, 250) );
                this.isFalling = true;    
            }
        }
        else if(this.isFalling) {
            if(pos.y>=160)
                this.vy += RunningMan.G ;
            else {
                this.vy = 0;
                this.setPosition( new cc.Point( pos.x,this.floorY) );
                this.isFalling = false;
                this.isJumping = false;
            }
        }
    },
    
    moveRight: function() {
        this.runAction( this.movingAction );
    },

    jump: function(){
        if(!this.isJumping) {
            cc.audioEngine.playEffect( 'res/jump.mp3' );
            this.isStart = true;
            this.isJumping = true;
        }
    },
    
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/dino1.png' );
        animation.addSpriteFrameWithFile( 'res/dino2.png' );
	    animation.setDelayPerUnit( 0.2 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },

    createDeathAnimation: function(){
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/dino0.png' );
        animation.setDelayPerUnit( 0.01 );
        return  cc.Animate.create( animation);
    },

    checkDeath: function() {
        if(this.isAlive == false){
            this.runAction(this.deathAction);
            this.vy = 0;
        }
    },

    getRect: function() {
        var spriteRect = this.getBoundingBoxToWorld();
        var spritePos = this.getPosition();

        var dX = this.x - spritePos.x;
        var dY = this.y - spritePos.y;
        return cc.rect(spriteRect.x + dX,
            spriteRect.y + dY,
            spriteRect.width-30,
            spriteRect.height-30);

    },

});
RunningMan.G = -1.5;
RunningMan.JUMP = 16; 