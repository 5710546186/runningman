var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );  
        this.scheduleUpdate();
        SpeedObstacle = -0;
        this.createBackGround();
        this.mainCharactor = new RunningMan();
        this.mainCharactor.setPosition( new cc.Point( 100, this.mainCharactor.floorY ) );
        this.addChild(this.mainCharactor,3);
        this.mainCharactor.scheduleUpdate();
        
        this.backgroundX = 0;
        this.background2X = 0;
        this.whichBackgroundShow = 1;

        this.bgLine = new PackBackGround();
        this.addChild(this.bgLine,1);

        this.cloud = new Cloud ();
        this.cloud.setPosition(screenWidth/2, (screenHeight*3/4)-12);
        this.addChild(this.cloud,2);

        this.addKeyboardHandlers();
        this.obstacle = new Obstacle(this,1);
        this.obstacle.setPosition( screenWidth + this.obstacle.width , 127 );
        this.addChild(this.obstacle,2 );

        this.countTime = 0;

        this.score = 0;
        

        this.scoreLabel = cc.LabelTTF.create('0', 'Arial', 25);
        this.scoreLabel.setPosition(new cc.Point(710, 400));
        this.addChild(this.scoreLabel);
        this.scoreLabel.setFontFillColor(new cc.Color(82,82,82));

        this.gameOver = cc.LabelTTF.create('', 'Arial', 30);
        this.gameOver.setPosition(screenWidth/2, screenHeight/2);
        this.addChild(this.gameOver,5);
        this.gameOver.setFontFillColor(new cc.Color(82,82,82));

	    return true;
    },
    
    update: function(dt) {
        this.countTime +=dt;
        if(this.countTime>0.3){
            this.countTime = 0;
            if(this.mainCharactor.isAlive == true&&this.mainCharactor.isStart == true){
                this.score += 1;
            }
            else if(this.mainCharactor.isAlive == false){
                this.gameOver.setString('GAME OVER');
            }
        }
        this.updateScore(this.score);
    },
    
    onKeyDown: function( e ) {
        switch(e){
            case cc.KEY.space:  
                if(!this.isPlaySelectSound){
                    cc.audioEngine.stopAllEffects();
                    this.mainCharactor.jump();
                }

                if(this.mainCharactor.isAlive == false){
                    this.restart();
                }

                break;
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            }
        }, this);
    },
    
    createBackGround: function() {
        this.bg = new BackGround();
        this.bg.setPosition( new cc.Point( screenWidth/2 ,screenHeight/2 ) );
        this.addChild(this.bg);
        this.bg.scheduleUpdate();;
        this.bg = this.bg.getPosition().x;
    },

    updateScore: function(score) {
        this.scoreLabel.setString("Score : " + score);

    },

    restart: function(){
        cc.director.runScene(new StartScene);

    }
    
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});

var SpeedObstacle = -7;