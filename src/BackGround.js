var PackBackGround = cc.Node.extend({
    ctor: function(layer) {
        this._super();
        this.scheduleUpdate();
        this.layer = layer;
        this.createBG();
    },

    createBG:function(){
        this.bg1 = new BGLine();
        this.bg2 = new BGLine();
        this.bg1.setPosition(screenWidth / 2, 109);
        this.bg2.setPosition(screenWidth * 1.5,109);
        this.addChild(this.bg1, 0);
        this.addChild(this.bg2, 0);

    },

    update: function() {
        this.move();
        this.loop();
    },
    loop: function() {
        if (-this.getPosition().x > screenWidth) {
            this.setPositionX(0);
        }
    },
    move: function() {
        this.x += SpeedObstacle
    },
});

var BackGround = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/background.jpg' );
    },
    update: function( dt ) {
	    
    }
});
var BGLine = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/line.png' );
        this.scheduleUpdate();
    },
    update: function( dt ) {
    }

 });   
var Cloud = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile( 'res/cloud.png' );
		this.scheduleUpdate();
	},
	update: function( dt ) {
		this.setPositionX(this.x+SpeedObstacle);
		if(this.x < - this.width){
			this.setPositionX(this.width*0.2+screenWidth);
		}
	}
});
