var Obstacle = cc.Sprite.extend({
    ctor: function(gameLayer,type) {
        this._super();
        if(type == 1){
        	this.initWithFile( 'res/Obstacle.png' );
    	}
    	else if(type == 2){
    		this.initWithFile( 'res/Obstacle2.png' );
    	}
 		this.gameLayer = gameLayer;
 		this.mainCharacter = this.gameLayer.mainCharactor;
        this.scheduleUpdate();
        this.typeObstacle = type;
        this.createOther = false;

    },

    update: function( dt ) {
	    this.setPositionX( this.x + SpeedObstacle);
	    if(this.x < -this.width){
	    	this.removeFromParent();
	    }
	    if(cc.rectOverlapsRect(this.getRect(),this.mainCharacter.getRect())){
	    	SpeedObstacle = 0;
	    	this.mainCharacter.isAlive = false;
	    }
	    this.createObstacle();
    },

    setNewRandomPosition: function() {       
    	var randomPosition = 1+Math.floor(Math.random()*4);
    	     
    	if(randomPosition == 1){
        	this.setPosition( screenWidth + this.width , 127 );
        }
        if(randomPosition == 2){
        	this.setPosition( screenWidth + screenWidth*2/4 + this.width , 127);
        }
        if(randomPosition == 3){
        	this.setPosition( screenWidth +this.width/2 , 127);
        }
        if(randomPosition == 4){
        	this.setPosition( screenWidth*1.2 + this.width , 127);
        }
    },

    createObstacle: function(){
	    if(this.x<screenWidth/2){
	    	if(this.createOther == false){
	    		var ran = 1+Math.floor(Math.random()*2);
	    		var obstacle = new Obstacle(this.gameLayer,ran);
	    		obstacle.setNewRandomPosition();
	    		this.gameLayer.addChild(obstacle,2);
	    		this.createOther = true;
	    	}
	    }

    },

    getRect: function(){
    	      var spriteRect = this.getBoundingBoxToWorld();
        var spritePos = this.getPosition();

        var dX = this.x - spritePos.x;
        var dY = this.y - spritePos.y;
        return cc.rect(spriteRect.x + dX,
            spriteRect.y + dY,
            spriteRect.width,
            spriteRect.height);

    },

});